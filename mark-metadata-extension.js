const ROOT = "data/2018-06-26--10-40-10"


// selector for the loader HTML element
const LOADER_SELECTOR = '#data-load-indicator2';

const DASHBOARD_NAME = "My first dashboard";

let MarkMetadataExtension = (function(){


  //
  // The list of all files in a data folder
  const DATA_FILES = {
    dataSourceColumns: "datasource-columns",
    dataSourceJoins: "datasource-joins",
    dataSourceTables: "datasource-tables",
    viewColumns: "view-columns",
    workbookDatasources: "workbook-datasources",
    workbookInfoComments: "workbook-info-comments",
    workbookInfoGeneral: "workbook-info-general",
    workbookInfoTags: "workbook-info-tags",
    workbookViews: "workbook-views",
  };

  function logObject(v) {
    console.log("[LOGOBJECT] " + JSON.stringify(v))
    return v;
  }

  //
  function init({tableau, root}) {
    let data = loadData(root);
    return data
      // .then(logObject)
      .catch(logObject)
  }

  function makeColumn({name, dataType, isReferenced}) {

  }

  function loadJson(url) {
    return new Promise((resolve, reject)=>{
      $.getJSON(url).done(resolve).fail(reject);
    });
  }

  function loadCsv(url) {
    return new Promise((resolve, reject)=>{
      // console.log("[PAPA] url=" + url);
      Papa.parse(url, {
        delimiter: ",",
        download: true,
        header: true,
        dynamicTyping: true,
        error: function(err, file, inputElem, reason) {
          console.error(...arguments);
          // return reject({err, file, inputElem, reason});
        },
        complete: results => {
          // console.log("[PAPA] complete=" + url);
          if (results.errors.length > 0) {
            console.log("while loading: ", url);
            return reject(results.errors);
          }
          return resolve(results.data);
        }
      });
    });
  }


  function $loader() { return $(LOADER_SELECTOR);  }
  function showLoader(v) { $('body').addClass('loading'); $loader().show(); return v; }
  function hideLoader(v) { $('body').removeClass('loading'); $loader().hide(); return v; }


  let DATA = _.mapObject(DATA_FILES, (k,v)=> []);

  /*
   * Returns a promise for loading all data from a root path
   */
  function loadData(root) {


    function pathForKey(f) {
      return root + "/" + f + ".csv";
    }

    // Loads a single file by finding the URL and returning
    // a key-value pair with the file's key as key and its contents as value
    function loadSingleFile(k) {
      let fileName = DATA_FILES[k];
      console.log("Starting load from" + pathForKey(k));
      return loadFile(fileName)
        .then(data => {
          console.log("[LOAD] " + k + " from: '" + pathForKey(k)   + "'" );
          return _.object([k], [data]);
        });
    }


    // loads a single file
    function loadFile(f) {
      let url = pathForKey(f);
      return loadCsv(url);
    }

    function flatten(all) {
      return Object.assign(...all);
    }

    // show the loader before doing anything
    showLoader();

    let allFiles = Object.keys(DATA_FILES).map(loadSingleFile);

    return Promise.all(allFiles).then(flatten)
      .then(data => {
        // assign the global data
        DATA = data;
        hideLoader();
        return data;
      });
  }

  function getDashboardName() {
    return DASHBOARD_NAME;
  }

  // Called on Mark Change Tableau event
  function onMarkChange(event) {
    // find the worksheet of the event
    let worksheet = event.worksheet;

    // fetch the marks selected
    let selectedMarks = event.getMarksAsync();

    // figure out the columns represented by the marks
    let dataTables = selectedMarks.then( marks => marks.data);

    // fetch each column for each mark
    let columnCollection = dataTables.then(tables => {
      return _.flatten(tables.map(table => table.columns.map(buildFieldData)));
    });

    function buildFieldData(col) {
      return {
        worksheet: worksheet,
        worksheetName: worksheet.name,
        column: col,
        name: col.fieldName,
      }
    }


    function buildAndUpdateAllNavs(fields) {
      let html = fields.map(buildNavHtml);

      $('#fieldNav').html(html);
      return fields;
    }

    function buildAndUpdateAllFields(fields) {
      let html = fields.map(buildMetadataHtml);

      $('#fieldData').html(html);
      return fields;
    }


    // Change the class of the body according to the presence of seleced columns
    function addSelectionClassToBody(fields) {
      const KLASS = 'has-selection';
      let $body = $('body');
      if (fields.length > 0) {
        $body.addClass(KLASS);
      } else {
        $body.removeClass(KLASS);
      }
      return fields;
    }


    return columnCollection.then(cols => {
      let allCols = cols.map(col => {
        let c = findColumnMetadata(DATA, col.worksheetName, col.name);
        let dashboard = tableau.extensions.dashboardContent.dashboard;

        return Promise.resolve({
          columnName: col.name,
          worksheetName: col.worksheetName,
          dashboard: getDashboardName(),
          data: c,
        })
      });

      return Promise.all(allCols);
    })
      .then(logObject)
      .then(buildAndUpdateAllNavs)
      .then(buildAndUpdateAllFields)
      .then(addSelectionClassToBody);

  }


  /**
   * Find the metadata from the view columns CSV
   */
  function findColumnMetadata(data, worksheetName, columnName) {
    let dashboard = tableau.extensions.dashboardContent.dashboard;
    let dashboardName = getDashboardName();

    function columnMatches(col) {
      return col.WORKBOOK_NAME === dashboardName &&
        // col.VIEW_NAME === worksheetName &&
        col.TABLEAU_DISPLAY_COLUMN === columnName;
    }
    return _.find( data.dataSourceColumns, columnMatches);
  }

  // Generates an HTML-id compatible string from a field name
  function columnNameId(name) {
    return name.replace(/[^a-zA-Z0-9-_]+/, '-');
  }


  // builds the navigation pill for a column
  function buildNavHtml(col) {

    let linkHtml = $('<a class="column-meta-link badge"></a>')
      .attr('href', '#column-meta-' + columnNameId(col.columnName))
      .text(col.columnName)
      .addClass( col.data ? 'badge-primary' : 'badge-light')
      .addClass( col.data ? 'has-data' : 'has-no-data')


    return linkHtml;
  }

  // Builds the HTML for the actual metadata table
  function buildMetadataHtml(col) {
    let colHtml = $('<div class="column-metadata"></div>');
    colHtml.addClass( col.data ? 'has-data' : 'has-no-data');

    colHtml
      .append('<a name="column-meta-' + columnNameId(col.columnName) + '"></a>')
      .append('<h4>' + col.columnName + ' <small class="column-worksheet">' + col.worksheetName + '</small></h4>')
      .append(buildJsonHtml(col.data));

    return colHtml;
  }



  function buildJsonHtml(o) {
    switch(typeof o) {
      case "undefined": return $.parseHTML('<span class="undefined"></span>');
      case "string": return $.parseHTML('<span class="string">' + o + '</span>');
      case "number": return $.parseHTML('<span class="number">' + o.toString() + '</span>');
      case "boolean": return $.parseHTML('<span class="boolean">' + (o ? "true" : "false") + '</span>');
      // case "array":
      //   return $('<ul></ul>')
      //     .append(o.map(child => $('<li></li>').append(child)));
      case "object": {
        if (o === null) {
          return $.parseHTML("NULL");
        }
        let html = $('<dl class="property-list"></dl>');
        Object.keys(o).forEach(key => {
          let child = o[key];
          let childHtml = buildJsonHtml(child);
          $('<dt></dt>').text(key).appendTo(html);
          $('<dd></dd>').append(childHtml).appendTo(html);
        });

        return html;
        // return $('<dl></dl>')
        //   .append(o.map(child => $('<li></li>').append(child)));
      }
      default:
        return $('<span class="unknown">' + o + '</span>');

    }
  }

  return {
    init: init,
    onMarkChange: onMarkChange,
  }
})();


function boot() {


  $(document).ready(function() {

    tableau.extensions.initializeAsync().then(function() {

    let app = MarkMetadataExtension.init({
        tableau: tableau,
        root: ROOT,
      });


    let dashboard = tableau.extensions.dashboardContent.dashboard;

    // Then loop through each worksheet and get its dataSources, save promise for later.
    let unregisterHandlers = dashboard.worksheets.map(function (worksheet) {
      let handler = MarkMetadataExtension.onMarkChange;
      return worksheet.addEventListener(tableau.TableauEventType.MarkSelectionChanged, handler);
    });

    // Display the name of dashboard in the UI
    // $("#resultBox").html("I'm running in a dashboard named <strong>" + dashboard.name + "</strong>");
  }, function(err) {

    // something went wrong in initialization
    $("#resultBox").html("Error while Initializing: " + err.toString());
  });
});

}
